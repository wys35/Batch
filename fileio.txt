A common technique is to use > to create/overwrite a log file, then use >> subsequently to append to the log file.
By default, the > and >> operators redirect stdout. You can redirect stderr by using the file number 2 in front of the operator:
PING 127.0.0.1 > NUL

DIR /B | SORT
